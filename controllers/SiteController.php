<?php

namespace app\controllers;
use yii\data\SqlDataProvider;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Ciclista;
use app\models\Lleva;
use app\models\Puerto;
use yii\data\ActiveDataProvider;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    /*
     * Creado esta accion
     */
    
    public function actionCrud(){
        return $this->render('gestion');
    }
    
    
    public function actionConsulta1a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("edad")->distinct(),
            'pagination'=>[
                'pageSize'=>5,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1",
            "enunciado"=>"Consulta las edadades de los ciclistas",
            "sql"=>"select distinct edad from ciclista",
        ]);
    }
    
    public function actionConsulta1(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand('SELECT count(DISTINCT c.edad) FROM ciclista c' )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> 'SELECT DISTINCT c.edad FROM ciclista c' ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>5,
                ],
            
            
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 1",
            "enunciado"=>"Consulta las edadades de los ciclistas",
            "sql"=>"select distinct edad from ciclista",
        ]);
                
    }
    
    public function actionConsulta2a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("edad")->distinct()->where("nomequipo='Artiach'"),
            'pagination'=>[
                'pageSize'=>5,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2",
            "enunciado"=>"Listar las edadades de los ciclistas de Artiach",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach'",
        ]);
    }
    
     public function actionConsulta2(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(DISTINCT edad) FROM ciclista WHERE nomequipo='artiach'" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT edad FROM ciclista WHERE nomequipo='artiach'" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>5,
                ],
            
            
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 2",
            "enunciado"=>"Consulta las edadades de los ciclistas de Artiach",
            "sql"=>"select distinct edad from ciclista where nomequipo='Artiach'",
        ]); 
        
    }
    
    public function actionConsulta3a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("edad")->distinct()
                ->where("nomequipo='artiach' OR nomequipo='Amore vita'"),
            'pagination'=>[
                'pageSize'=>5,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3",
            "enunciado"=>"listar las edadades de los ciclista del equipo de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='artiach' OR nomequipo='Amore vita'",
        ]);
    }
    
     public function actionConsulta3(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(DISTINCT edad) FROM ciclista WHERE nomequipo='artiach' OR nomequipo='Amore vita'" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT edad FROM ciclista WHERE nomequipo='artiach' OR nomequipo='Amore vita'" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>5,
                ],
            
            
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['edad'],
            "titulo"=>"Consulta 3",
            "enunciado"=>"listar las edadades de los ciclista del equipo de Artiach o de Amore Vita",
            "sql"=>"SELECT DISTINCT edad FROM ciclista WHERE nomequipo='artiach' OR nomequipo='Amore vita'",
        ]); 
    }
    
    public function actionConsulta4a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("dorsal")->distinct()
                ->where("edad < 25 or edad >30"),
            'pagination'=>[
                'pageSize'=>5,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor 25 o mayor que 30",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad < 25 or edad >30",
        ]);
    }
    
     public function actionConsulta4(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(DISTINCT dorsal) FROM ciclista WHERE edad < 25 or edad >30" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT dorsal FROM ciclista WHERE edad < 25 or edad >30" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>5,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 4",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad sea menor 25 o mayor que 30",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad < 25 or edad >30",
        ]);
    }
    
       public function actionConsulta5a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("dorsal")->distinct()
                ->where("edad  >= 28 AND edad <=32 AND nomequipo='Banesto'"),
            'pagination'=>[
                'pageSize'=>5,
                ]
        ]);
                
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esta entre 28 y 32 y además que solo sean de Banesto",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='Banesto'",
        ]);
    }
    
     public function actionConsulta5(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(DISTINCT dorsal) FROM ciclista WHERE edad < 25 and edad <30" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT dorsal FROM ciclista WHERE edad < 25 and edad <30" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>5,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 5",
            "enunciado"=>"Listar los dorsales de los ciclistas cuya edad esta entre 28 y 32 y además que solo sean de Banesto",
            "sql"=>"SELECT dorsal FROM ciclista WHERE edad BETWEEN 28 AND 32 AND nomequipo='Banesto'",
        ]);
    } 
    
     public function actionConsulta6a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("nombre")->distinct()
                ->where("CHAR_LENGTH(nombre)>8"),
            'pagination'=>[
                'pageSize'=>30,
                ]
        ]);
                
          return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6",
            "enunciado"=>"Listar nombre de los ciclistas que el número de carácteres sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8",
        ]);
    }
    
     public function actionConsulta6(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(DISTINCT nombre) FROM ciclista WHERE CHAR_LENGTH(nombre)>8" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>30,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre'],
            "titulo"=>"Consulta 6",
            "enunciado"=>"Listar nombre de los ciclistas que el número de carácteres sea mayor que 8",
            "sql"=>"SELECT DISTINCT nombre FROM ciclista WHERE CHAR_LENGTH(nombre)>8",
        ]);
    } 
    
    
    public function actionConsulta7a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Ciclista::find()->select("upper(nombre) mayusculas,nombre")->distinct(),
            'pagination'=>[
                'pageSize'=>30,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','mayusculas'],
            "titulo"=>"Consulta 7",
            "enunciado"=>"Listar el nombre y el dorsal de todos los ciclistas mostrando un campo denominao nombre mayúscula",
            "sql"=>"SELECT DISTINCT nombre, UPPER(NOMBRE) AS 'nombre mayúsculas' FROM ciclista",
        ]);
    }
    
     public function actionConsulta7(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(DISTINCT nombre) FROM ciclista" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT nombre, UPPER(NOMBRE) AS 'nombre mayúsculas' FROM ciclista" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>30,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nombre','nombre mayúsculas'],
            "titulo"=>"Consulta 7",
            "enunciado"=>"Listar el nombre y el dorsal de todos los ciclistas mostrando un campo denominao nombre mayúscula",
            "sql"=>"SELECT DISTINCT nombre, UPPER(NOMBRE) AS 'nombre mayúsculas' FROM ciclista",
        ]);
    } 
    
    public function actionConsulta8a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Lleva::find()->select(" dorsal")->distinct()->where ("código='MGE'")
                ,
            'pagination'=>[
                'pageSize'=>10,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8",
            "enunciado"=>"Listar los ciclistas que han llevdo el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT dorsal FROM lleva WHERE código='MGE'",
        ]);
    }
    
     public function actionConsulta8(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(DISTINCT dorsal) FROM lleva WHERE código='MGE'" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT dorsal FROM lleva WHERE código='MGE'" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>10,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 8",
            "enunciado"=>"Listar los ciclistas que han llevdo el maillot MGE (amarillo) en alguna etapa",
            "sql"=>"SELECT dorsal FROM lleva WHERE código='MGE'",
        ]);
    } 
    
    public function actionConsulta9a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()->select("nompuerto")->where ("altura<1500")
                ,
            'pagination'=>[
                'pageSize'=>3,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9",
            "enunciado"=>"Lista el nombre de los puertos cuya altura sea mayor 1500",
            "sql"=>"SELECT  nompuerto  FROM puerto WHERE altura<1500",
        ]);
    }
    
     public function actionConsulta9(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(DISTINCT dorsal) FROM lleva WHERE código='MGE'" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT  nompuerto  FROM puerto WHERE altura<1500" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>3,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['nompuerto'],
            "titulo"=>"Consulta 9",
            "enunciado"=>"Lista el nombre de los puertos cuya altura sea mayor 1500",
            "sql"=>"SELECT  nompuerto  FROM puerto WHERE altura<1500",
        ]);
    } 
    
     public function actionConsulta10a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()->select("dorsal")->distinct()->where ("pendiente>8 OR altura BETWEEN 1800 AND 3000")
                ,
            'pagination'=>[
                'pageSize'=>3,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto curya pendiente se mayor que 8 o cuya altura este entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT  dorsal  FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000",
        ]);
    }
    
     public function actionConsulta10(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(DISTINCT dorsal) FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT  dorsal  FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>3,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 10",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto curya pendiente se mayor que 8 o cuya altura este entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT  dorsal  FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000",
        ]);
    }
    
    public function actionConsulta11a(){
        // mediante active record
        $dataProvider=new ActiveDataProvider([
            'query'=> Puerto::find()->select("dorsal")->distinct()->where ("pendiente>8 AND altura BETWEEN 1800 AND 3000")
                ,
            'pagination'=>[
                'pageSize'=>3,
                ]
        ]);
                
           return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto curya pendiente se mayor que 8 y cuya altura este entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT  dorsal  FROM puerto WHERE pendiente>8 OR altura BETWEEN 1800 AND 3000",
        ]);
    }
    
     public function actionConsulta11(){
            // mediante DAO
        $numero=Yii::$app
                ->db->createCommand("SELECT count(DISTINCT dorsal) FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000" )
                ->queryScalar(); // cuenta los registros que hay en la consulta y lo guarda $numero
        
        $dataProvider= new SqlDataProvider([
            'sql'=> "SELECT DISTINCT  dorsal  FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000" ,
            'totalCount' => $numero, // se lo pasamos de arriba
            'pagination'=>[
                'pageSize'=>3,
                ],
        ]);
        
        return $this->render("resultado",[
            "resultados"=>$dataProvider,
            "campos"=>['dorsal'],
            "titulo"=>"Consulta 11",
            "enunciado"=>"Listar el dorsal de los ciclistas que hayan ganado algun puerto curya pendiente se mayor que 8 y cuya altura este entre 1800 y 3000",
            "sql"=>"SELECT DISTINCT  dorsal  FROM puerto WHERE pendiente>8 AND altura BETWEEN 1800 AND 3000",
        ]);
    }
    
    
    
    
    
    
     }
