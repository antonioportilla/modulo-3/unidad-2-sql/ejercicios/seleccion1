<?php
use yii\bootstrap\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>

<div class="site-index">

    <div class="jumbotron">
        <h1>consultas de seleccion 1</h1>
pplication
        <p class="lead">Modulo 3 Unidad 2</p>
    </div>

    <div class="body-content">

        <div class="row">
            <!-- primera consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 1</h3>
                      <p>Listar las edadades de ciclists sin repetidos</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta1a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta1'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
            <!-- segunda consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 2</h3>
                      <p>Listar las edadades de ciclists de Artiach</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta2a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta2'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
             <!-- tercera consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 3</h3>
                      <p>Listar las edadades de ciclists de Artiach o de Amore Vita</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta3a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta3'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
             
              <!-- cuarta consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 4</h3>
                      <p>Listar los dorsales de los ciclistas cuya edad sea menor 25 o mayor que 30</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta4a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta4'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
               <!-- quinta consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 5</h3>
                      <p>Listar los dorsales de los ciclistas cuya edad esta entre 28 y 32 y además que solo sean de Banesto</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta5a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta5'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
               <!-- sexta consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 6</h3>
                      <p>Listar nombre de los ciclistas que el número de carácteres sea mayor que 8</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta6a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta6'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
               
              <!-- septima consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 7</h3>
                      <p>Listar el nombre y el dorsal de todos los ciclistas mostrando un campo denominao nombre mayúscula</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta7a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta7'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              <!-- octava consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 8</h3>
                      <p>Listar los ciclistas que han llevdo el maillot MGE (amarillo) en alguna etapa</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta8a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta8'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              <!-- novena consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 9</h3>
                      <p>Lista el nombre de los puertos cuya altura sea mayor 1500</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta9a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta9'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              <!-- decima consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 10</h3>
                      <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto curya pendiente se mayor que 8 o cuya altura este entre 1800 y 3000</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta10a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta10'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              <!-- undecima consulta  -->
            
                <div class="col-sm-6 col-md-4">
                  <div class="thumbnail">
                    <div class="caption">
                      <h3>Consulta 11</h3>
                      <p>Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 y cuya altura este entre 1800 y 3000</p>
                      <p>
                          <?= Html::a('Active Record',['site/consulta11a'],['class' => 'btn btn-primary btn-large']);?>
                          <?= Html::a('DAO',['site/consulta11'],['class' => 'btn btn-default btn-large']);?>
                      </p>
                    </div>
                  </div>
                </div>
              
        </div>

    </div>
</div>
